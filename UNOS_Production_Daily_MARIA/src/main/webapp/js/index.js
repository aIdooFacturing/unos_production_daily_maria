var pmcDTyList = [];
var tyList = [
              {
            	  "id" : "Adapter",
            	  "name" : "Adapter"
              },
              {
            	  "id" : "IO Logik",	
            	  "name" : "IO Logik"
              }
              ];

function getPcmDTy(){
	var url = ctxPath + "/getPcmDTy.do";
	
	$.ajax({
		url : url,
		dataType : "json",
		async :false,
		type : "post",
		success : function(data){
			var json = data.dataList;
			
			$(json).each(function(idx, data){
				var obj = {};
				obj.id = data.id;
				obj.name = data.name;
				
				pmcDTyList.push(obj)
			})
		}
	});
};

function pmcTyName(id, arry){
	for (var i = 0; i < arry.length; i++) {
		if (arry[i].id == id) {
			return arry[i].name;
		}
    }
};

function tyDropDownEditor(container, options){
	$('<input required name="' + options.field + '"/>')
	 .appendTo(container)
	 .kendoDropDownList({
		 autoBind: true,
		 dataTextField: "name",
		 dataValueField: "name",
		 dataSource: tyList
	});
}

function pmcTyDropDownEditor(container, options){
	$('<input required name="' + options.field + '"/>')
	 .appendTo(container)
	 .kendoDropDownList({
		 autoBind: true,
		 dataTextField: "name",
		 dataValueField: "id",
		 dataSource: pmcDTyList
	});
}

$(function(){
	getPcmDTy();
});

var nc_obj = [];
var update_nc_obj = [];

var selectedRow;
function drawGrid(){
	var url = ctxPath + "/getNcSetting.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			var json = data.dataList;

			$("#grid").kendoGrid({
		        dataSource: new kendo.data.DataSource({
		            data: json,
		            batch: true,
		            schema: {
		                   model: {
		                     id: "dvcId",
		                     fields: {
		                        dvcId: { editable: false},
		                        name: { editable: true},
		                        ty : { editable: true},
		                        ncIpAddr : { editable : true},
		                        ncPort: { editable : true },
		                        cncTy: { editable : true },
		                        opcAddr : { editable : true },
		                        ioAddr : { editable : true },
		                        dataParsingInterval : { editable : true },
		                        mainCycleProgram : { editable : true},
		                        finishCd : { editable : true },  
		                        dataSendInterval: { editable : true },
		                        pmcDTy: { editable : true },
		                        pmcStart: { editable : true },
		                        pmcEnd: { editable : true }
		                     }
		                   }
		               },
		        pageSize: 10,
		    	}),
		    	change: function() {
		    		  var cell = this.select();
		    		  var cellIndex = cell[0].cellIndex;
		    		  var column = this.columns[cellIndex];
		    		  var dataItem = this.dataItem(cell.closest("tr"));
				        		  
		    		  selectedRow = dataItem    		  
		    		
		    	},
		    		  
		    	selectable: "row",
		    	
		        height : getElSize(1650),
		        pageable: true,
		        toolbar: [{text : "ADD", icon : "k-add", className : "add_row"},
		                  {text : "SAVE", className : "save_row"},
		                  {text : "DELETE", className : "del_row"}],
		        groupable: false,
		        sortable: true,
		        pageable: {
		            refresh: true,
		            pageSizes: true,
		            buttonCount: 5
		        },
		        columns: [
							{
								field : "dvcId"
								,title : "선택"
								,template: "<input type='checkbox' class='checkbox'  />" 
								,width : getElSize(170) + "px"
								,attributes: {
		                			style: "text-align: center;"
		              			}
							},
		        			{
		            			field: "name",
		        				title: "Name",
		        				width : getElSize(370) + "px",
		            			attributes: {
		                			style: "text-align: center; font-size: " + getElSize(35) + "px"
		              			}
		        			},
		        			{
		            			field: "ty",
		        				title: "Type",
		        				editor: tyDropDownEditor,
		            			template: "#= pmcTyName(ty, tyList) #",
		        				width : getElSize(370) + "px",
		            			attributes: {
		                			style: "text-align: center; font-size: " + getElSize(35) + "px"
		              			}
		        			},
							{
		            			field: "ncIpAddr",
		        				title: "NC_IP_Address",
		        				width : getElSize(370) + "px",
		            			attributes: {
		                			style: "text-align: center; font-size: " + getElSize(35) + "px"
		              			}
		        			},
		                  	{
		            			field: "ncPort",
		            			title: "NC_Port",
		            			width : getElSize(370) + "px",
		            			attributes: {
		                			style: "text-align: center; font-size: " + getElSize(35) + "px"
		              			}
		        			}, 
		        			{
		            			field: "cncTy",
		        				title: "CNC_Interface",
		        				width : getElSize(370) + "px",
		            			attributes: {
		                			style: "text-align: center; font-size: " + getElSize(35) + "px"
		              			}
		        			},
		        			{
		            			field: "opcAddr",
		            			title: "OPC_Address",
		            			width : getElSize(370) + "px",
		            			attributes: {
		                			style: "text-align: center; font-size: " + getElSize(35) + "px"
		              			}
		        			},
		        			{
		            			field: "ioAddr",
		            			title: "IO_Address",
		            			width : getElSize(370) + "px",
		            			attributes: {
		                			style: "text-align: center; font-size: " + getElSize(35) + "px"
		              			}
		        			},
		        			{
		            			field: "dataParsingInterval",
		        				title: "Data_Parsing_Interval", 
		        				width : getElSize(420) + "px",
		            			attributes: {
		                			style: "text-align: center; font-size: " + getElSize(35) + "px"
		              			}
		        			},
		        			{
		            			field: "mainCycleProgram",
		        				title: "Main_Program",
		        				width : getElSize(370) + "px",
		            			attributes: {
		                			style: "text-align: center; font-size: " + getElSize(35) + "px"
		              			}
		        			},
		        			{
		            			field: "finishCd",
		        				title: "Finish Code",
		        				width : getElSize(370) + "px",
		            			attributes: {
		                			style: "text-align: center; font-size: " + getElSize(35) + "px"
		              			}
		        			},
		        			{
		            			field: "dataSendInterval",
		            			title: "Data_Send_Interval",
		            			width : getElSize(470) + "px",
		            			attributes: {
		                			style: "text-align: center; font-size: " + getElSize(35) + "px"
		              			}
		        			},
		        			{
		            			field: "pmcDTy",
		            			title: "PMC_Data_Type",
		            			width : getElSize(370) + "px",
		            			editor: pmcTyDropDownEditor,
		            			template: "#= pmcTyName(pmcDTy, pmcDTyList) #",
		            			attributes: {
		                			style: "text-align: center; font-size: " + getElSize(35) + "px"
		              			}
		        			},
		        			{
		            			field: "pmcStart",
		            			title: "PMC_Start_Address",
		            			width : getElSize(420) + "px",
		            			attributes: {
		                			style: "text-align: center; font-size: " + getElSize(35) + "px"
		              			}
		        			},
		        			{
		            			field: "pmcEnd",
		            			title: "PMC_End_Address",
		            			width : getElSize(420) + "px",
		            			attributes: {
		                			style: "text-align: center; font-size: " + getElSize(35) + "px"
		              			}
		        			}
		        		],
		        		editable: true
		        		
		    });
			
			$("#grid").css("width",$(".menu_right").width())
			
			$("#grid th").css({
				"font-size" : getElSize(35) + "px",
				"text-align" : "center"
			})
			
//			$(".k-button").css({
//				"padding" : getElSize(10)
//			})
			
			$(".k-grid").css({
				"border" : 0
			})
			
			$(".checkbox").css({
				"width" : getElSize(70) + "px",
				"height" : getElSize(70) + "px"
			})
			grid = $("#grid").data("kendoGrid");
			grid.table.on("click", ".checkbox" , selectRow);
			
			$("#grid td").on("keyup", function (e) {
//		        //if current key is Enter
//		        var row = $(this).closest("tr");
//		        grid = $("#grid").data("kendoGrid"),
//		        dataItem = grid.dataItem(row);
//				        
//		        console.log(dataItem.cnt,dataItem.prc)
//				dataItem.set("totalPrc", dataItem.cnt * dataItem.prc)    		  
			})

			$(".add_row, .save_row, .del_row").unbind("click")
			
			$(".add_row").click(function(){
				checkedIds = {}
				grid.dataSource.add({
					"선택" : "<input type='checkbox' class='checkbox' />"
					,"ty" : "Adapter"
					,"pmcDTy" : 1
					,"opcAddr" : "opc.tcp://0.0.0.0:0000"
					,"ioAddr" : "http://0.0.0.0/01.htm"
				});	
			});
			
			$(".save_row").click(addRow);
			$(".del_row").click(delRow);
		}
	});
};

function addRow(){
	nc_obj = [];
	update_nc_obj = [];
	
	var $checked = $('input[type=checkbox]:checked');
	
	if($checked.length==0){
		alert("항목을 선택하지 않으셨습니다.")
		return
	};
	
	$($checked).each(function(idx, data){
		var row = $(data).closest("tr");
		var dataItem = grid.dataItem(row);
		
		var dvcId = dataItem.dvcId;
		var name = dataItem.name;
		var ncIpAddr = dataItem.ncIpAddr;
		var ncPort = dataItem.ncPort;
		var cncTy = dataItem.cncTy;
		var opcAddr = dataItem.opcAddr;
		var ioAddr = dataItem.ioAddr;
		var dataParsingInterval = dataItem.dataParsingInterval;
		var mainCycleProgram = dataItem.mainCycleProgram;
		var finishCd = dataItem.finishCd;
		var dataSendInterval = dataItem.dataSendInterval;
		var pmcDTy = dataItem.pmcDTy;
		var pmcStart = dataItem.pmcStart;
		var pmcEnd = dataItem.pmcEnd;
		var ty = dataItem.ty;
		
		if(typeof(name)=="undefined"){
			name = "";
		}
		if(typeof(ncIpAddr)=="undefined"){
			ncIpAddr = "";
		}
		
		if(typeof(ncPort)=="undefined"){
			ncPort = "";
		}
		
		if(typeof(cncTy)=="undefined"){
			cncTy = "";
		}
		if(typeof(opcAddr)=="undefined"){
			opcAddr = "";
		}
		if(typeof(ioAddr)=="undefined"){
			ioAddr = "";
		}
		if(typeof(dataParsingInterval)=="undefined"){
			dataParsingInterval = "";
		}
		if(typeof(mainCycleProgram)=="undefined"){
			mainCycleProgram = "";
		}
		if(typeof(finishCd)=="undefined"){
			finishCd = "";
		}
		if(typeof(dataSendInterval)=="undefined"){
			dataSendInterval = "";
		}
		if(typeof(pmcDTy)=="undefined"){
			pmcDTy = "";
		}
		if(typeof(pmcStart)=="undefined"){
			pmcStart = "";
		}
		if(typeof(pmcEnd)=="undefined"){
			pmcEnd = "";
		}
		if(typeof(ty)=="undefined"){
			ty = "";
		}
			
		var object = {};
		
		object.dvcId = dvcId;
		object.name = name;
		object.ncIpAddr = ncIpAddr;
		object.ncPort = ncPort;
		object.cncTy = cncTy;
		object.opcAddr = opcAddr;
		object.ioAddr = ioAddr;
		object.dataParsingInterval = dataParsingInterval;
		object.mainCycleProgram = mainCycleProgram;
		object.finishCd = finishCd;
		object.dataSendInterval = dataSendInterval;
		object.pmcDTy = pmcDTy;
		object.pmcStart = pmcStart;
		object.pmcEnd = pmcEnd;
		object.shopId = shopId;
		object.ty = ty;
		
		
		if(typeof(dvcId)=="undefined"){
			nc_obj.push(object)
		}else{
			update_nc_obj.push(object)
		}	
	});
	
	
	var obj = new Object();
	obj.val = nc_obj;

	var url = ctxPath + "/addNcSetting.do";
	
	var param = "val=" + JSON.stringify(obj);
	
	if(update_nc_obj.length!=0) {
		updateRow()
	}
	
	if(nc_obj.length!=0){
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				console.log(data)
				drawGrid()
			},error : function(e1,e2,e3){
				console.log(e1,e2,e3)
			}
		});
	}
};

function updateRow(){
	var obj = new Object();
	obj.val = update_nc_obj;

	var url = ctxPath + "/updateNcSetting.do";
	
	var param = "val=" + JSON.stringify(obj);
	
	var str;
	$.ajax({
		url : url,
		async : false,
		data : param,
		type : "post",
		dataType : "text",
		success : function(data){
			str = data
			console.log(data)
			if(nc_obj.length==0){
				drawGrid()
			}
		}
	});
	
	return str;
};

var blank_rows = [];
function delRow(){
	if(!confirm($chkDel)) return;
		
	var $checked = $('input[type=checkbox]:checked');
	var rows = []
	$($checked).each(function(idx, data){
		var checked = data;
		var row = $(data).closest("tr");
        dataItem = grid.dataItem(row);
        rows.push(dataItem);
        
        var dvcId = dataItem.dvcId;
        
        var obj = {};
        obj.dvcId = dvcId;
        
        if(typeof(dvcId)!="undefined"){//저장 된 row
        	nc_obj.push(obj)
        }else{// 저장 되지 않은 row
        	blank_rows.push(dataItem)
        }
	})
	
	$(blank_rows).each(function(idx, data){
		grid.dataSource.remove(data)
	});
	
	if(nc_obj.length==0) return;
	var obj = {};
	obj.val = nc_obj;
	
	var url = ctxPath + "/delNcSetting.do";
	var param = "val=" + JSON.stringify(obj);
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "text",
		success : function(data){
			console.log(data)
			if(data=="success"){
				$(rows).each(function(idx, data){
					grid.dataSource.remove(data)
				});
			}
		}
	});
}